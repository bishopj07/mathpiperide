/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.functions.core;

import org.mathpiper.builtin.BuiltinFunction;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class BackQuote extends BuiltinFunction
{
    
    private BackQuote()
    {
    }

    public BackQuote(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        org.mathpiper.lisp.astprocessors.BackQuoteSubstitute behaviour = new org.mathpiper.lisp.astprocessors.BackQuoteSubstitute(aEnvironment);

        Cons result = Utility.substitute(aEnvironment, aStackTop, getArgument(aEnvironment, aStackTop, 1), behaviour);
        
        setTopOfStack(aEnvironment, aStackTop, aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, result));
    }
}



/*
%mathpiper_docs,name="`",categories="Operators"
*A {`}
*CMD Backquoting --- macro expansion (LISP-style backquoting)
*CORE
*CALL
	`(expression)

*PARMS

{expression} -- expression containing "{@var}" combinations to substitute the value of variable "{var}"

*DESC

Backquoting is a macro substitution mechanism. A backquoted {expression}
is evaluated in two stages: first, variables prefixed by {@} are evaluated
inside an expression, and second, the new expression is evaluated.

To invoke this functionality, a backquote {`} needs to be placed in front of
an expression. Parentheses around the expression are needed because the
backquote binds tighter than other operators.

The expression should contain some variables (assigned atoms) with the special
prefix operator {@}. Variables prefixed by {@} will be evaluated even if they
are inside function arguments that are normally not evaluated (e.g. functions
declared with {HoldArgument}). If the {@var} pair is in place of a function name,
e.g. "{@f(x)}", then at the first stage of evaluation the function name itself
is replaced, not the return value of the function (see example); so at the
second stage of evaluation, a new function may be called.

One way to view backquoting is to view it as a parametric expression
generator. {@var} pairs get substituted with the value of the variable {var}
even in contexts where nothing would be evaluated. This effect can be also
achieved using {ListToFunction} and {Hold} but the resulting code is much more
difficult to read and maintain.

This operation is relatively slow since a new expression is built
before it is evaluated, but nonetheless backquoting is a powerful mechanism
that sometimes allows to greatly simplify code.

*E.G.

This example defines a function that automatically evaluates to a number as
soon as the argument is a number (a lot of functions  do this only when inside
a {NM(...)} section).

In> Decl(f1,f2) :=  `(@f1(x_Number?) <-- NM(@f2(x)));
Result: True;

PKHG_TODO 17-11-2013: better example?! See the quote before sin!
In> Decl('sin,SinN)
Result: True

In> sin(1)
Result: 0.8414709848

PKHG_TODO 17-11-2013 Sin does not exist now
In> Decl(nSin,Sin)
Result: True;

In> Sin(1)
Result: Sin(1);


In> nSin(1)
Result: 0.8414709848;

This example assigns the expression {func(value)} to variable {var}. Normally
the first argument of {Assign} would be unevaluated.

In> SetF(var,func,value) := `(Assign(@var,@func(@value)));
Result: True;

In> SetF(a,Sin,x)
Result: True;

In> a
Result: Sin(x);


*SEE MacroAssign, MacroLocal, MacroRulebaseHoldArguments, Hold, HoldArgument, MacroRulebaseHoldArguments, MacroExpand
%/mathpiper_docs
*/