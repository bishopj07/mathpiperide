/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.functions.core;


import org.mathpiper.builtin.BuiltinFunction;
import static org.mathpiper.builtin.BuiltinFunction.getArgument;
import static org.mathpiper.builtin.BuiltinFunction.setTopOfStack;
import org.mathpiper.exceptions.BreakException;
import org.mathpiper.exceptions.ContinueException;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;


/**
 *
 *  
 */
public class For extends BuiltinFunction
{

    private For()
    {
    }

    public For(String functionName)
    {
        this.functionName = functionName;
    }
//_start,_predicate,_increment,_body

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        
        Cons startArg = getArgument(aEnvironment, aStackTop, 1);
        Cons predicateArg = getArgument(aEnvironment, aStackTop, 2);
        Cons incrementArg = getArgument(aEnvironment, aStackTop, 3);
        Cons bodyArg = getArgument(aEnvironment, aStackTop, 4);

        aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, startArg);
        
        Cons predicate = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, predicateArg);

        Cons evaluated = Utility.getFalseAtom(aEnvironment);

        int beforeStackTop = -1;
        int beforeEvaluationDepth = -1;
        
        try {
            while (Utility.isTrue(aEnvironment, predicate, aStackTop)) {

                beforeStackTop = aEnvironment.iArgumentStack.getStackTopIndex();
                beforeEvaluationDepth = aEnvironment.iEvalDepth;

                try {

                    evaluated = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, bodyArg);

                } catch (ContinueException ce) {
                    aEnvironment.iArgumentStack.popTo(beforeStackTop, aStackTop, aEnvironment);
                    aEnvironment.iEvalDepth = beforeEvaluationDepth;
                    setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
                }//end continue catch.
                finally
                {
                    aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, incrementArg);
                }

                predicate = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, predicateArg);

            }//end while.

            if(! Utility.isFalse(aEnvironment, predicate, aStackTop)) LispError.checkArgument(aEnvironment, aStackTop, 1);

        } catch (BreakException be) {
              aEnvironment.iArgumentStack.popTo(beforeStackTop, aStackTop, aEnvironment);
              aEnvironment.iEvalDepth = beforeEvaluationDepth;
        }

        setTopOfStack(aEnvironment, aStackTop, evaluated);
    }


}

/*
%mathpiper_docs,name="For",categories="Programming Procedures;Control Flow"
*CMD For --- C-style [for] loop
*STD
*CALL
        For(init, pred, incr) body

*PARMS

{init} -- expression for performing the initialization

{pred} -- predicate deciding whether to continue the loop

{incr} -- expression to increment the counter

{body} -- expression to loop over

*DESC

This commands implements a C style {for} loop. First
of all, the expression "init" is evaluated. Then the predicate
"pred" is evaluated, which should return {True} or
{False}. Next the loop is executed as long as the
predicate yields {True}. One traversal of the loop
consists of the subsequent evaluations of "body", "incr", and
"pred". Finally, the value {True} is returned.

This command is most often used in a form such as {For(i := 1, i <=? 10, i++) body}, which evaluates {body} with
{i} subsequently set to 1, 2, 3, 4, 5, 6, 7, 8, 9,
and 10.

The expression {For(init, pred, incr) body} is
equivalent to {init; While(pred) {body; incr;}}.

*E.G. notest

In> For(i:=1, i<=?10, i++) Echo([i, i!]);
         1  1
         2  2
         3  6
         4  24
         5  120
         6  720
         7  5040
         8  40320
         9  362880
         10  3628800
Result: True;

*SEE While, Until, ForEach, Break, Continue
%/mathpiper_docs
 */
