/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.functions.core;

import org.mathpiper.builtin.BuiltinFunction;
import static org.mathpiper.builtin.BuiltinFunction.getArgument;
import static org.mathpiper.builtin.BuiltinFunction.setTopOfStack;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class ListToString extends BuiltinFunction
{

    private ListToString()
    {
    }

    public ListToString(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {       
        Cons argument1 = (Cons) getArgument(aEnvironment, aStackTop, 1).car();
        
        if(! Utility.isList(argument1)) LispError.throwError(aEnvironment, aStackTop, "The first argument must be a list");
        
        Object argument2 = getArgument(aEnvironment, aStackTop, 2).car();
        
        if(! Utility.isString(argument2)) LispError.throwError(aEnvironment, aStackTop, "The first argument must be a string");
        
        String separator = Utility.stripEndQuotesIfPresent((String) argument2);
  
        StringBuilder sb = new StringBuilder();

        argument1 = argument1.cdr();
        
        while (argument1 != null) {
            
            sb.append(Utility.stripEndQuotesIfPresent(Utility.toString(aEnvironment, aStackTop, argument1)));
            
	    argument1 = argument1.cdr();
            
            if(argument1 != null)
            {
                sb.append(separator);
            }

	}
                
                
        setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), Utility.toMathPiperString(aEnvironment, aStackTop, sb.toString())));
        
        return;
  
        
        


    }//end method..

}//end class.



/*
%mathpiper_docs,name="ListToString",categories="Programming Procedures;Lists (Operations)",access="experimental"
*CMD ListToString --- converts a list into a string
*STD
*CALL
        ListToString(list, separator)

*PARMS

{list} -- a list to be converted into a string
{separator} -- a string that is inserted between the list elements

*DESC
This function converts each of the elementes in a list into a string and then concatenates these
strings into a single string.

*E.G.
In> ListToString(['a,'b,'c,'d], "")
Result: "abcd"

*SEE StringToList
%/mathpiper_docs
*/
