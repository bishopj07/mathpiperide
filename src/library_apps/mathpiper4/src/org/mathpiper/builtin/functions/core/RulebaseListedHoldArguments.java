/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.functions.core;

import org.mathpiper.builtin.BuiltinFunction;
import org.mathpiper.lisp.Environment;

/**
 *
 *  
 */
public class RulebaseListedHoldArguments extends BuiltinFunction
{

    private RulebaseListedHoldArguments()
    {
    }

    public RulebaseListedHoldArguments(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        org.mathpiper.lisp.Utility.defineRulebase(aEnvironment, aStackTop, true);
    }
}



/*
%mathpiper_docs,name="RulebaseListedHoldArguments",categories="Programming Procedures;Miscellaneous;Built In"
*CMD RulebaseListedHoldArguments --- define function with variable number of arguments
*CORE
*CALL
	RulebaseListedHoldArguments("name", params)

*PARMS

{"name"} -- string, name of function

{params} -- list of arguments to function

*DESC

The command {RulebaseListedHoldArguments} defines a new function. It essentially works the
same way as {RulebaseHoldArguments}, except that it declares a new function with a variable
number of arguments. The list of parameters {params} determines the smallest
number of arguments that the new function will accept. If the number of
arguments passed to the new function is larger than the number of parameters in
{params}, then the last argument actually passed to the new function will be a
list containing all the remaining arguments.

A function defined using {RulebaseListedHoldArguments} will appear to have the arity equal
to the number of parameters in the {param} list, and it can accept any number
of arguments greater or equal than that. As a consequence, it will be impossible to define a 
new function with the same name and with a greater arity.

The function body will know that the function is passed more arguments than the
length of the {param} list, because the last argument will then be a list. The
rest then works like a {Rulebase}-defined function with a fixed number of
arguments. Transformation rules can be defined for the new function as usual.


*E.G.

The definitions

    RulebaseListedHoldArguments("f",["a", "b", "c"]);
    10 # f(_a,_b,[_c,_d]) <-- Echo(["four args",a,b,c,d]);
    20 # f(_a,_b,c_List?) <-- Echo(["more than four args",a,b,c]);
    30 # f(_a,_b,_c) <-- Echo(["three args",a,b,c]);
	
give the following interaction:
In> Function() f(_a); Function() f(_a,_b);

In> f(_A) 
Result: f(_A);

In> f(_A,_B) 
Result: f(_A,_B);

In> f(_A,_B,_C)
Result: True;
Side Effects:
three args _A _B _C

In> f(_A,_B,_C,_D)
Result: True
Side Effects:
four args _A _B _C _D

In> f(_A,_B,_C,_D,_E)
Result: True
Side Effects:
more than four args _A _B [_C,_D,_E]

In> f(_A,_B,_C,_D,_E,_E)
Result: True
Side Effects:
more than four args _A _B [_C,_D,_E,_E]

The function {f} now appears to occupy all arities greater than 3:

In> RulebaseHoldArguments("f", {x,y,z,t});
	CommandLine(1) : Rule base with this arity already defined


*SEE RulebaseHoldArguments, Retract, Echo
%/mathpiper_docs
*/
