/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.functions.core;

import org.mathpiper.builtin.BuiltinFunction;
import static org.mathpiper.builtin.BuiltinFunction.getArgument;
import static org.mathpiper.builtin.BuiltinFunction.setTopOfStack;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class UnderscoreConstant_ extends BuiltinFunction
{
    
    private UnderscoreConstant_()
    {
    }

    public UnderscoreConstant_(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons result = getArgument(aEnvironment, aStackTop, 1);
        
        Object object = result.car();
        
        if(! (object instanceof String))
        {
            setTopOfStack(aEnvironment, aStackTop, Utility.getFalseAtom(aEnvironment));
            
            return;
        }
        
        String name = (String) object;
        
        boolean isVariable = name.contains("_");
        
        setTopOfStack(aEnvironment, aStackTop, Utility.getBooleanAtom(aEnvironment, isVariable));
    }
}




/*
%mathpiper_docs,name="UnderscoreConstant?",categories="Programming Procedures;Predicates"
*CMD UnderscoreConstant? --- test for an underscore constant
*STD
*CALL
        UnderscoreConstant?(expression)

*PARMS

{expression} -- expression to test

*DESC

This procedure tests if the argument is an underscore constant.

*E.G.

In> UnderscoreConstant?(_x)
Result: True

%/mathpiper_docs




%mathpiper,name="Variable?",subtype="automatic_test"

Verify(UnderscoreConstant?('a), False);
Verify(Variable?(_a), True);

%/mathpiper
*/