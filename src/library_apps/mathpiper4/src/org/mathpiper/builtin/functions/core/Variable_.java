/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.functions.core;

import org.mathpiper.builtin.BuiltinFunction;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;


public class Variable_ extends BuiltinFunction
{
    
    private Variable_()
    {
    }

    public Variable_(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons result = getArgument(aEnvironment, aStackTop, 1);
        
        Object object = result.car();
        
        if(! (object instanceof String))
        {
            setTopOfStack(aEnvironment, aStackTop, Utility.getFalseAtom(aEnvironment));
            
            return;
        }
        
        String name = (String) object;
        
        boolean isVariable = (aEnvironment.isVariable(name));
        
        /*
        if(name.contains("_"))
        {
            isVariable = true;
        }
        */
        
        setTopOfStack(aEnvironment, aStackTop, Utility.getBooleanAtom(aEnvironment, isVariable));
    }
}



/*
%mathpiper_docs,name="Variable?",categories="Programming Procedures;Predicates"
*CMD Variable? --- test for a variable
*STD
*CALL
        Variable?(var)

*PARMS

{var} -- variable to test

*DESC

This procedure tests if the argument is a variable.

*E.G.

In> Unassign(x)
Result: True

In> Variable?(x)
Result: True

In> x := 3
Result: 3

In> Variable?(x)
Result: False

In> Variable?(Infinity)
Result: False

*SEE VarList, VarListAll
%/mathpiper_docs




%mathpiper,name="Variable?",subtype="automatic_test"

Verify(Variable?('a), True);
Verify(Variable?(_a), False);
Verify(Variable?(Sqrt(_a)), False);
Verify(Variable?(2), False);
Verify(Variable?(-2), False);
Verify(Variable?(2.1), False);

%/mathpiper
*/
