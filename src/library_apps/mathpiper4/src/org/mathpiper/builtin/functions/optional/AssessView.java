/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.functions.optional;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.mathpiper.builtin.BuiltinFunction;
import static org.mathpiper.builtin.BuiltinFunction.getArgument;
import static org.mathpiper.builtin.BuiltinFunction.setTopOfStack;
import org.mathpiper.builtin.BuiltinFunctionEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.worksheets.AssessPanel;

/**
 *
 *
 */
public class AssessView extends BuiltinFunction implements ResponseListener {

    private Map defaultOptions;
    private List<ResponseListener> responseListeners = new ArrayList<ResponseListener>();
    
    public void plugIn(Environment aEnvironment)  throws Throwable
    {
	this.functionName = "AssessView";
	
        aEnvironment.getBuiltinFunctions().put(
                this.functionName, new BuiltinFunctionEvaluator(this, 1, BuiltinFunctionEvaluator.VariableNumberOfArguments | BuiltinFunctionEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("Scale", 1.0);
        defaultOptions.put("Resizable", false);
        defaultOptions.put("Hint", false);

    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
	
        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if(! Utility.isSublist(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.
        
        Object firstArgument = arguments.car();
        
        if(! (firstArgument instanceof JavaObject))
        {
           LispError.throwError(aEnvironment, aStackTop, "The first argument must be a Java Component.");
        }

        Object object = ((JavaObject) firstArgument).getObject();
        
        if (! (object instanceof Component))
        {
            LispError.throwError(aEnvironment, aStackTop, "The first argument must be a Java Component.");
        }
                
        final Component componentFinal = (Component) object;


        Cons options = arguments.cdr();
        Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        boolean isHint = (Boolean) userOptions.get("Hint");
        //Double viewScale = ((Double)userOptions.get("Scale")).doubleValue();
        

        AssessPanel assessPanel = new AssessPanel(isHint);
        
        assessPanel.add(componentFinal);
        
        List responseList = new ArrayList();
        responseList.add(assessPanel);
        responseList.add(assessPanel);
        JavaObject response = new JavaObject(responseList);

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));


    }//end method.
    
    
    

    
    
    public void response(EvaluationResponse response) {
        notifyListeners(response);
    }
    
    public boolean remove()
    {
        return false;
    }
    
    public void addResponseListener(ResponseListener listener) {
        responseListeners.add(listener);
    }

    public void removeResponseListener(ResponseListener listener) {
        responseListeners.remove(listener);
    }

    protected void notifyListeners(EvaluationResponse response) {
        for (ResponseListener listener : responseListeners) {
            listener.response(response);
        }//end for.
    }

}//end class.





/*
%mathpiper_docs,name="AssessView",categories="Programming Procedures;Assessment"
*CMD AssessView --- display an assessment-related GUI

*CALL
    AssessView(javaComponent, option, option, option...)

*PARMS
{javaComponent} -- a java Component

{Options:}


*DESC
Display a Java Component.

 
*E.G.
In> 

%/mathpiper_docs
*/



