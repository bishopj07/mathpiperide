/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.functions.optional;

import java.util.Arrays;
import org.mathpiper.builtin.BuiltinFunction;
import static org.mathpiper.builtin.BuiltinFunction.getArgument;
import static org.mathpiper.builtin.BuiltinFunction.setTopOfStack;
import org.mathpiper.builtin.BuiltinFunctionEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *
 */
public class HistogramPrint extends BuiltinFunction
{

    public void plugIn(Environment aEnvironment) throws Throwable
    {
        this.functionName = "HistogramPrint";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinFunctionEvaluator(this, 4, BuiltinFunctionEvaluator.FixedNumberOfArguments | BuiltinFunctionEvaluator.EvaluateArguments));
    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
       Cons argument1 = (Cons) getArgument(aEnvironment, aStackTop, 1).car();

        if (!Utility.isList(argument1))
        {
            LispError.throwError(aEnvironment, aStackTop, "The argument must be a list");
        }

        double[] data = JavaObject.lispListToJavaDoubleArray(aEnvironment, aStackTop, argument1);
        
        int min = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 2).toInt();
        
        int max = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 3).toInt();
        
        int bucketSize = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 4).toInt();
        
        int[] buckets = histogram(aEnvironment, aStackTop, data, min, max, bucketSize);

        String plot = displayHistogram(min, bucketSize, max, buckets);
        
        aEnvironment.iCurrentOutput.write(plot);
        
        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }

    static int[] histogram(Environment aEnvironment, int aStackTop, double[] data,  int binMinimum,  int binMaximum,  int binSize) throws Throwable
    {
        // note, in this method, there's no need to know the actual values, just the range.
        final int range = binMaximum - binMinimum + 1;
        final int buckets = (range + binSize - 1) / binSize;
        final int[] results = new int[buckets];
        
        for (int i = 0; i < data.length; i++)
        {
            double value = data[i];

            value = value -.0000001; //todo:tk:an experimental adjustment to get integer values placed into the correct bins.

            int bucketIndex = (int)value / binSize;

            if(bucketIndex >= results.length) 
            {
                LispError.throwError(aEnvironment, aStackTop, "The third argument must be larger than the largest number in the list.");
            }
            
            results[bucketIndex]++;
        }
        return results;
    }

    public static String displayHistogram(final int first, final int numberOfbins, final int maxbar, final int[] bins)
    {
        // Obtained from http://ideone.com/O9k6EX
        StringBuilder sb = new StringBuilder();
        int maxcount = 0;
        for(int num:bins)
        {
            if(num > maxcount)
            {
                maxcount = num;
            }
        }
        int scale = ((maxcount + maxbar - 1) / maxbar) + 1;
        int width = (maxcount + scale - 1) / scale;
        String format = "%5d-%-5d | %-" + width + "s | %d\n";
        for (int i = 0; i < bins.length; i++)
        {
            int start = (numberOfbins * i) + first;
            int end = start + numberOfbins - 1;
            //System.out.printf(format, start, end, buildAsterisks(buckets[i], scale), buckets[i]);
            sb.append(String.format(format, start, end, buildAsterisks(bins[i], scale), bins[i]));
        }

        sb.append(String.format("%11s | %s\n", "", lines(width)));
        sb.append(String.format("%-11s | %s\n", "Scale", marks(maxcount, scale)));

        return sb.toString();
    }

    private static Object marks(int maxcount, int scale)
    {
        int width = (int) Math.log10(maxcount - 1) + 2;
        int count = ((maxcount + scale - 1) / scale) / width;
        StringBuilder sb = new StringBuilder();
        int mark = 0;
        int step = scale * width;
        for (int i = 0; i < count; i++)
        {
            mark += step;
            sb.append(String.format("%" + width + "d", mark));
        }
        return sb.toString();
    }

    private static String lines(int width)
    {
        char[] array = new char[width];
        Arrays.fill(array, '-');
        return new String(array);
    }

    private static String buildAsterisks(int size, int scale)
    {
        char[] array = new char[(size + scale - 1) / scale];
        Arrays.fill(array, '*');
        return new String(array);
    }



}//end class.

/*
 %mathpiper_docs,name="HistogramPrint",categories="Programming Procedures;Visualization;Built In",access="experimental"
 *CMD HistogramPrint --- prints a histogram

 *CALL
 HistogramPrint(data, binMinimum, binMaximum, numberOfBins)

 *PARMS
 {data} -- a list of decimal numbers
 {binMinumum} -- the minimum bin value 
 {binMaximum} -- the maximum bin value
 {numberOfBins} -- the number of bins

 *DESC
 Prints a horizontal histogram. Note: does not work correctly
yet for decimal numbers.

 *E.G.
 In> HistogramPrint(RandomIntegerList(20,1,7), 1, 7, 2)
 Result: True
 Side Effects:
     1-2     | **** | 7
     3-4     | ***  | 6
     5-6     | **   | 4
     7-8     | **   | 3
             | ----
 Scale       |  4 8

 %/mathpiper_docs
 */
