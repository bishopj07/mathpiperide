%mathpiper,def="/"

/* Division */

50 # 0 / 0 <-- Undefined;

52 # x_PositiveNumber? / 0 <-- Infinity;
52 # x_NegativeNumber? / 0 <-- -Infinity;
55 # (x_ / y_Number?)::(Zero?(y)) <-- Undefined;
55 # 0 / x_ <-- 0;
// unnecessary rule (see #100 below). TODO: REMOVE
//55 # x_Number? / y_NegativeNumber? <-- (-x)/(-y);

56 # (x_NonZeroInteger? / y_NonZeroInteger?)::(GcdN(x,y) >? 1) <--
     {
       Local(gcd);
       Assign(x,x);
       Assign(y,y);
       Assign(gcd,GcdN(x,y));
       QuotientN(x,gcd)/QuotientN(y,gcd);
     };

57 # ((x_NonZeroInteger? * expr_) / y_NonZeroInteger?)::(GcdN(x,y) >? 1) <--
     {
       Local(gcd);
       Assign(x,x);
       Assign(y,y);
       Assign(gcd,GcdN(x,y));
       (QuotientN(x,gcd)*expr)/QuotientN(y,gcd);
     };

57 # ((x_NonZeroInteger?) / (y_NonZeroInteger? * expr_))::(GcdN(x,y) >? 1) <--
     {
       Local(gcd);
       Assign(x,x);
       Assign(y,y);
       Assign(gcd,GcdN(x,y));
       QuotientN(x,gcd)/(QuotientN(y,gcd)*expr);
     };

57 # ((x_NonZeroInteger? * p_) / (y_NonZeroInteger? * q_))::(GcdN(x,y) >? 1) <--
     {
       Local(gcd);
       Assign(x,x);
       Assign(y,y);
       Assign(gcd,GcdN(x,y));
       (QuotientN(x,gcd)*p)/(QuotientN(y,gcd)*q);
     };

60 # (x_Decimal? / y_Number?)  <-- DivideN(x,y);
60 # (x_Number?  / y_Decimal?) <-- DivideN(x,y); 
60 # (x_Number?  / y_Number?)::(NumericMode?()) <-- DivideN(x,y);


90 # x_Infinity? / y_Infinity? <-- Undefined;
95  # x_Infinity? / y_Number? <-- Sign(y)*x;
95  # x_Infinity? / y_Complex? <-- Infinity;

90 # Undefined / y_ <-- Undefined;
90 # y_ / Undefined <-- Undefined;


100 # x_ / x_ <-- 1;
100 # x_ /  1 <-- x;
100 # (x_ / y_NegativeNumber?) <-- -x/(-y);
100 # (x_ / - y_) <-- -x/y;

//100 # Tan(x_)/Sin(x_) <-- (1/Cos(x));

//100 # 1/Csch(x_)        <-- Sinh(x);

//100 # Sin(x_)/Tan(x_) <-- Cos(x);

//100 # Sin(x_)/Cos(x_) <-- Tan(x);

//100 # 1/Sech(x_) <-- Cosh(x);

//100 # 1/Sec(x_)                <-- Cos(x);

//100 # 1/Csc(x_)                <-- Sin(x);

//100 # Cos(x_)/Sin(x_) <-- (1/Tan(x));

//100 # 1/Cot(x_) <-- Tan(x);

//100 # 1/Coth(x_)        <-- Tanh(x);

//100 # Sinh(x_)/Cosh(x_) <-- Tanh(x);

150 # (x_) / (x_) ^ (n_Constant?) <-- x^(1-n);
150 # Sqrt(x_) / (x_) ^ (n_Constant?) <-- x^(1/2-n);
150 # (x_) ^ (n_Constant?) / Sqrt(x_) <-- x^(n-1/2);
150 # (x_) / Sqrt(x_) <-- Sqrt(x);

// fractions
200 # (x_ / y_)/ z_ <-- x/(y*z);
230 # x_ / (y_ / z_) <-- (x*z)/y;

240 # (xlist_List? / ylist_List?)::(Length(xlist)=?Length(ylist)) <--
         Map("/",[xlist,ylist]);


250 # (x_List? / y_)::(Not?(List?(y))) <--
{
    Local(i,result);

    Check(x !=? [], "An arithmetic operation can't be done with an empty list.");

    result:=[];

    For(i:=1,i<=?Length(x),i++)
    {
        Insert!(result,i,x[i] / y); 
    };
    
    result;
};

250 # (x_ / y_List?)::(Not?(List?(x))) <--
{
    Local(i,result);

    Check(y !=? [], "An arithmetic operation can't be done with an empty list.");

    result:=[];

    For(i:=1,i<=?Length(y),i++)
    {
        Insert!(result,i,x/y[i]);
    };

    result;
};

250 # x_ / Infinity <-- 0;
250 # x_ / (-Infinity) <-- 0;


400 # 0 / x_ <-- 0;

400 # x_RationalOrNumber? / Sqrt(y_RationalOrNumber?)  <-- Sign(x)*Sqrt(x^2/y);
400 # Sqrt(y_RationalOrNumber?) / x_RationalOrNumber?  <-- Sign(x)*Sqrt(y/(x^2));
400 # Sqrt(y_RationalOrNumber?) / Sqrt(x_RationalOrNumber?)  <-- Sqrt(y/x);

%/mathpiper


%mathpiper_docs,name="/",categories="Operators"
*CMD / --- arithmetic division
*STD
*CALL
        x/y

*PARMS

{x} and {y} -- objects for which arithmetic division is defined

*DESC

The division operator can work on integers,
rational numbers, complex numbers, vectors, matrices and lists.

This operator is implemented in the standard math library (as opposed
to being built-in). This means that they can be extended by the user.

*E.G.

In> 6/2
Result: 3;

In> 0/0
Result: Undefined

%/mathpiper_docs





%mathpiper,name="/",subtype="automatic_test"

Verify(Infinity/Infinity,Undefined);
Verify(0.0/Sqrt(2),0);
Verify(0.0000000000/Sqrt(2),0);

%/mathpiper