%mathpiper,def="Association"

/* Association : given an assoc list like for example l:=[[a,2],[b,3]],
   Association(b,l) will return [b,3]. if the key is not in the list,
   it will return the atom None.
*/

Procedure("Association",["key", "list"])  BuiltinAssociation(key,list);

%/mathpiper



%mathpiper_docs,name="Association",categories="Programming Procedures;Lists (Operations)"
*CMD Association --- return element stored in association list
*STD
*CALL
        Association(key, alist)

*PARMS

{key} -- string, key under which element is stored

{alist} -- association list to examine

*DESC

The association list "alist" is searched for an entry stored with
index "key". If such an entry is found, it is returned. Otherwise
the atom {None} is returned.

Association lists are represented as a list of two-entry lists. The
first element in the two-entry list is the key, the second element is
the value stored under this key.

The call {Association(key, alist)} can (probably more
intuitively) be accessed as {alist[key]}.

*E.G.

In> writer := [];
Result: [];

In> writer["Iliad"] := "Homer";
Result: True;

In> writer["Henry IV"] := "Shakespeare";
Result: True;

In> writer["Ulysses"] := "James Joyce";
Result: True;

In> Association("Henry IV", writer);
Result: ["Henry IV","Shakespeare"];

In> Association("War and Peace", writer);
Result: None;

*SEE AssociationIndices, :=, AssociationDelete, AssociationValues
%/mathpiper_docs