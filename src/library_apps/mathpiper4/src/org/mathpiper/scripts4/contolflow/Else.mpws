%mathpiper,def="Else"

LocalSymbols(predicate, body, predicateEvaluated)
{
	RulebaseHoldArguments("Else",["ifthen", "otherwise"]);
	
	(If(predicate_) body_ Else otherwise_) <--
	{
		predicateEvaluated := Eval(predicate);
		
		Check(Boolean?(predicateEvaluated), "The first argument must be a predicate expression.");
		
		Decide(predicateEvaluated =? True, Eval(body),  Eval(otherwise));
	};
	

	HoldArgument("Else","ifthen");
	
	HoldArgument("Else","otherwise");
	
	UnFence("Else",2);
};

%/mathpiper




%mathpiper_docs,name="Else",categories="Programming Procedures;Control Flow"
*CMD Else --- used with the If function to make decisions

*CALL
    If(predicate) body Else otherwise

*PARMS

{predicate} -- predicate to test

{body} -- expression to evaluate if the predicate is {True}.

{otherwise} -- expression to evaluate if the predicate if {False}.

*DESC

An If() function can be used with an Else operator to evaluate one body 
if a predicate expression is True, and an alternative body if the predicate 
expression is False.

*E.G.
/%mathpiper

x := 4;

If(x >? 5)
{
    Echo(x,"is greater than 5.");
}
Else
{
    Echo(x,"is NOT greater than 5.");
};

Echo("End of program.");

/%/mathpiper

    /%output,preserve="false"
      Result: True
      
      Side Effects:
      4 is NOT greater than 5.
      End of program.
    /%/output

*SEE If, Decide
%/mathpiper_docs
