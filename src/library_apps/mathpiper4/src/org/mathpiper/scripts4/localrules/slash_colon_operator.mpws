%mathpiper,def="/:"


10 # (expression_ /: patterns_) <--
{
  Assign(patterns, PatternsCompile(patterns));
  
  MacroSubstituteApply(expression,"LocPredicate","LocChange");
};

%/mathpiper



%mathpiper_docs,name="/:",categories="Operators"
*CMD /: --- local transformation rules

        expression /: patterns

*PARMS

{expression} -- an expression

{patterns} -- a list of patterns

*DESC

Sometimes you have an expression, and you want to use specific
transformation rules on it that are not done by default. This
can be done with the {/:} and the {/::} operators. Suppose we have the
expression containing things such as {Ln(a*b)}, and we want
to change these into {Ln(a)+Ln(b)}, the easiest way
to do this is using the {/:} operator, as follows:

PKHG: 
In> Sin(_x)*Ln(_a*_b) /: [ Ln(_x*_y) <- Ln(x)+Ln(y) ]
Result: Sin(_x)*(Ln(_a)+Ln(_b))

In> Sin(_x)*Ln(_a*_b)
Result: Sin(_x)*Ln(_a*_b);

In> # /: [ Ln(_x*_y) <- Ln(x)+Ln(y) ]
Result: Sin(_x)*(Ln(_a)+Ln(_b));

A whole list of simplification rules can be built up in the list,
and they will be applied to the expression on the left hand side
of {/:} .

The forms the patterns can have are one of:

        pattern <- replacement
        {pattern,replacement}
        {pattern,postpredicate,replacement}

Note that for these local rules, {<-} should be used instead of
{<--} which would be used in a global rule.

The {/:} operator traverses an expression much as {Substitute} does, that is, top
down, trying to apply the rules from the beginning of the list of
rules to the end of the list of rules. If the rules cannot be applied
to an expression, it will try subexpressions of that
expression and so on.

A local rule that is defined by the <- operator can be given a name by using the 
# operator:

"RuleName" # Ln(_x*_y) <- Ln(x) + Ln(y)

Each time the rule is selected while the system is in verbose mode, the name of 
the rule will be printed as a side effect.


*E.G.

In> Sin(_u)*Ln(_a*_b) /: [Ln(_x*_y) <- Ln(x) + Ln(y)]
Result: Sin(u)*(Ln(a)+Ln(b));

In> Verbose(Sin(u)*Ln(a*b) /: [ "RuleName" # Ln(_x*_y) <- Ln(x) + Ln(y)])
Result: Sin(u)*(Ln(a)+Ln(b))
Side Effects:
RuleName


/%mathpiper

Hold((a + b) * (1 + 2) * (2 + 1) * (1/2 + c) * (3/4 + d) ) /:
   [
     (x_Odd? + y_Even?) <- m1_,
     
     (x_Even? + y_Odd?) <- m2_,
     
     (x_Rational? + y_Variable?)::(Denominator(x) =? 2) <- m3_
   ];

/%/mathpiper

    /%output,preserve="false"
      Result: ((((a + b) * m1_) * m2_) * m3_) * (3 / 4 + d)
.   /%/output

*SEE /::, Substitute
%/mathpiper_docs







%mathpiper,name="/:",subtype="automatic_test"

RulebaseHoldArguments("sqrt", ["x"]);
Verify(_a*Sqrt(_x)/:[Sqrt(_x) <- sqrt(_x)],_a*sqrt(_x));
Retract("sqrt", All);

%/mathpiper