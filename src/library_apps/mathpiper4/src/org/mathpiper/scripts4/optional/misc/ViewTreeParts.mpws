%mathpiper,def="ViewTreeParts;PositionsLeaves"

PositionsLeaves(expression) :=
{
	Local(stack, positions, subexpressionAndPosition, subexpression, index, argumentsCount);
	
	stack := [[expression, []]];
	
	positions := [];
	
	While(Length(stack) !=? 0)
	{
		subexpressionAndPosition := PopFront(stack);
		
		subexpression := First(subexpressionAndPosition);				
		
		If(!? Atom?(subexpression) &? (argumentsCount := ArgumentsCount(subexpression)) >? 0)
		{
			index := 1;
			
			While(index <=? argumentsCount)
			{
				Append!(stack, [subexpression[index], Append!(FlatCopy(subexpressionAndPosition[2]), index)]);
				
				index++;
			};
		}
		Else
		{
			Append!(positions, ListToString(subexpressionAndPosition[2], ","));
		};
		
		positions;
	};
};



Retract("ViewTreeParts", All);

RulebaseListedHoldArguments("ViewTreeParts", ["tree", "optionsList"]);

//Handle no options call.
5 # ViewTreeParts(tree_) <-- ViewTreeParts(tree, []);


//Main routine.  It will automatically accept two or more option calls because the
//options come in a list.
10 # ViewTreeParts(tree_, optionsList_List?) <--
{

    Local(list,
    	result, 
    	options, 
    	treeScale, 
    	latexScale, 
    	showPositions, 
    	function, 
    	path, 
    	pattern, 
    	process,
    	arcsHighlight?,
    	arcsAutoHighlight?,
    	resizable?,
    	showDepths?,
    	showPositions?, 
    	rootNodeHighlight?, 
    	nodesHighlight?, 
    	leafNodesHighlight?,
    	fontSize,
    	positionHighlight,
    	pathNumbers?,
    	code?,
    	debug?,
    	width,
    	height,
        center?);
    
    result := [];

    options := OptionsToAssociationList(optionsList);

    treeScale := Decide(Number?(options["Scale"]), options["Scale"], 2.0);
    fontSize := Decide(Number?(options["FontSize"]), options["FontSize"], 30.0);
    showPositions? := Decide(Boolean?(options["ShowPositions"]), options["ShowPositions"], False);
    showDepths? := Decide(Boolean?(options["ShowDepths"]), options["ShowDepths"], False);
    resizable? := Decide(Boolean?(options["Resizable"]), options["Resizable"], False);
    arcsHighlight? := Decide(Boolean?(options["ArcsHighlight"]), options["ArcsHighlight"], False);
    arcsAutoHighlight? := Decide(Boolean?(options["ArcsAutoHighlight"]), options["ArcsAutoHighlight"], True);
    nodesHighlight? := Decide(Boolean?(options["NodesHighlight"]), options["NodesHighlight"], False);
    leafNodesHighlight? := Decide(Boolean?(options["LeafNodesHighlight"]), options["LeafNodesHighlight"], False);
    rootNodeHighlight? := Decide(Boolean?(options["RootNodeHighlight"]), options["RootNodeHighlight"], False);
    includeExpression? := Decide(Boolean?(options["IncludeExpression"]), options["IncludeExpression"], True);
    pattern := options["Pattern"];
    path := options["Path"];
    nodeHighlight := options["NodeHighlight"];
    pathNumbers? := Decide(Boolean?(options["PathNumbers"]), options["PathNumbers"], True);
    code? := Decide(Boolean?(options["Code"]), options["Code"], False);
    debug? := Decide(Boolean?(options["Debug"]), options["Debug"], False);
    width := Decide(Integer?(options["Width"]), options["Width"], Null);
    height := Decide(Integer?(options["Height"]), options["Height"], Null);
    center? := Decide(Boolean?(options["Center"]), options["Center"], False);

    
    If(!? code?)
    {
    	tree := ObjectToMeta(tree);
    };
    

	Local(list, result);
	
	process := [
        [
        	"function",
            Lambda([trackingList,positionString,node], 
            {   
                MetaSet(node,"HighlightColor","ORANGE");

                node; 
            })
        ]
    ];
    function := ` '(TreeProcess("expression", "pattern", @process, Position:"position"));

	If(path !=? None)
	{
		Show(TreeView(tree, Process:function, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathHighlight:path, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?), returnContent:True, Width:width, Height:height, Center:center?);
	}
	Else If(pattern !=? None)
	{
		list := [
			["track",[]],
			
			["function",
				Lambda([trackingList, positionString, node], 
				{
					Append!(trackingList["track"], [ToString(node), positionString]);
					
					MetaSet(node,"HighlightColor","ORANGE");
					
					node; 
				})
			]
		];
		
		ForEach(entry, list["track"])
		{
			Echo("Subtree: " ~ entry[1] ~ ", Position: " ~ entry[2]);
		};
		
		result := TreeProcess(MetaToObject(tree), pattern, list);
		
		Show(TreeView(result, Process:function, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?), returnContent:True, Width:width, Height:height, Center:center?);
	}	
	Else If(leafNodesHighlight?)
	{
		Local(leafPositions);
		
		leafPositions := PositionsLeaves(tree);
		ForEach(leafPosition, leafPositions)
		{
			tree := Mark(tree, leafPosition, a_Atom?, "ORANGE");
		};
		
		Show(TreeView(tree, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?), returnContent:True, Width:width, Height:height, Center:center?);
	}
	Else If(rootNodeHighlight?)
	{
		tree := Mark(tree, "", a_Atom?, "ORANGE");
		
		Show(TreeView(tree, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?), returnContent:True, Width:width, Height:height, Center:center?);
	}
	Else If(nodeHighlight !=? None)
	{
		If(Atom?(nodeHighlight))
		{
			nodeHighlight := [nodeHighlight];
		};
		
		ForEach(position, nodeHighlight)
		{
			tree := Mark(tree, position, a_Atom?, "ORANGE");
		};
		
		Show(TreeView(tree, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?), returnContent:True, Width:width, Height:height, Center:center?);
	}
	Else
	{
		Show(TreeView(tree, Code:code?, Debug:debug?, Resizable:resizable?, Scale:treeScale, FontSize:fontSize, ShowPositions:showPositions?, ShowDepths:showDepths?, ArcsHighlight:arcsHighlight?, ArcsAutoHighlight:arcsAutoHighlight?, NodesHighlight:nodesHighlight?, PathNumbers:pathNumbers?, IncludeExpression:includeExpression?), returnContent:True, Width:width, Height:height, Center:center?);
	};
	
};


//Handle a single option call because the option does not come in a list for some reason.
20 # ViewTreeParts(tree_, singleOption_) <-- ViewTreeParts(tree, [singleOption]);



//ViewTreeParts('((a+b)* (c+d+a)), Pattern:(a_ + b_));

//ViewTreeParts('((a+b)* (c+d+a)),  ShowDepths:True);

//ViewTreeParts('((a+b)* (c+d+a)), Path:["11","12"]);

//ViewTreeParts('((a+b)* (c+d+3)), ArcsHighlight:True);

//ViewTreeParts('((a+b)* (c+d+3)), LeafNodesHighlight:True);

//ViewTreeParts('((a+b)* (c+d+3)), RootNodeHighlight:True);

//ViewTreeParts('((a+b)* (c+d+3)), NodesHighlight:True);

%/mathpiper

    %output,mpversion=".210",preserve="false"
      Result: True
.   %/output