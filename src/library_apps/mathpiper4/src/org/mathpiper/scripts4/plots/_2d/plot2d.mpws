%mathpiper,def="Plot2D;Plot2Dgetdata;Plot2Dadaptive"

//////////////////////////////////////////////////
/// Plot2D --- adaptive two-dimensional plotting
//////////////////////////////////////////////////

/// definitions of backends
//LoadScriptOnce("plots.rep/backends_2d.mpi");

/*
        Plot2D is an interface for various backends (Plot2D...). It calls
Plot2Dgetdata to obtain the list of points and values, and then it calls
Plot2D<backend> on that data.

        Algorithm for Plot2Dgetdata:
        1) Split the given interval into Quotient(points+3, 4) subintervals, and split each subinterval into 4 parts.
        2) For each of the parts: evaluate function values and call Plot2Dadaptive
        3) concatenate resulting lists and return
*/

LocalSymbols(var, func, range, option, optionslist, delta, optionshash, c, fc, allvalues)
{

// declaration of Plot2D with variable number of arguments
Procedure() Plot2D("func");
Procedure() Plot2D("func", "range");
Procedure() Plot2D("func", "range", "options", ...);

/// interface routines
1 # Plot2D(func_) <-- ("Plot2D" @ [func, -5->5]);
2 # Plot2D(func_, range_) <-- ("Plot2D" @ [func, range, []]);
3 # Plot2D(func_, range_, option_Function?):: (Type(option) =? ":" )  <-- ("Plot2D" @ [func, range, [option]]);

/// Plot a single function
5 # Plot2D(func_, range_, optionslist_List?)::(!? List?(func)) <-- ("Plot2D" @ [[func], range, optionslist]);

/// Top-level 2D plotting routine:
/// plot several functions sharing the same xrange and other options
4 # Plot2D(funclist_List?, range_, optionslist_List?) <--
{
        Local(var, func, delta, optionshash, c, fc, allvalues);
        allvalues := [];
        optionshash := "OptionsListToHash" @ [optionslist];
    
    
        // this will be a string - name of independent variable
        optionshash["xname"] := "";
        // this will be a list of strings - printed forms of functions being plotted
        optionshash["yname"] := [];
        // parse range
        
        /*
        Default plotting range is {-5->5}. Range can also be specified as {x= -5->5} (note the mandatory space separating "{=}" and "{-}");
        currently the variable name {x} is ignored in this case.
        Decide(
                Type(range) =? "==",        // variable also specified -- ignore for now, store in options
                {
                        // store alternative variable name
                        optionshash["xname"] := ToString(range[1]);
                        range := range[2];
                }
        );
        Verify(PipeToString()Write(Plot2D(b,b: -1->1,output: data,points: 4)), "[[[-1,-1],[-0.5,-0.5],[0.0,0.0],[0.5,0.5],[1,1]]]");
        */
        Decide(
                Type(range) =? "->",        // simple range
                range := NM(Eval([range[1], range[2]]))
        );
        // set default option values
        Decide(
                optionshash["points"] =? None,
                optionshash["points"] := 23
        );
        Decide(
                optionshash["depth"] =? None,
                optionshash["depth"] := 5
        );
        Decide(
                optionshash["precision"] =? None,
                optionshash["precision"] := 0.0001
        );
        Decide(
                optionshash["output"] =? None |? String?(optionshash["output"]) &? Plot2DOutputs()[optionshash["output"]] =? None,
                optionshash["output"] := Plot2DOutputs()["default"]
        );
        // a "filename" parameter is required when using data file
        Decide(
                optionshash["output"] =? "datafile" &? optionshash["filename"] =? None,
                optionshash["filename"] := "output.data"
        );
    
        // we will divide each subinterval in 4 parts, so divide number of points by 4 now
        optionshash["points"] := NM(Eval(Quotient(optionshash["points"]+3, 4)));
    
        // in case it is not a simple number but an unevaluated expression
        optionshash["precision"] := NM(Eval(optionshash["precision"]));
    
        // store range in options
        optionshash["xrange"] := [range[1], range[2]];
    
        // compute the separation between grid points
        delta := NM(Eval( (range[2] - range[1]) / (optionshash["points"]) ));

        // check that the input parameters are valid (all numbers)
        Check(Number?(range[1]) &? Number?(range[2]) &? Number?(optionshash["points"]) &? Number?(optionshash["precision"]),
                "Plot2D: Error: plotting range "
                ~(PipeToString()Write(range))
                ~" and/or the number of points "
                ~(PipeToString()Write(optionshash["points"]))
                ~" and/or precision "
                ~(PipeToString()Write(optionshash["precision"]))
                ~" is not numeric"
        );
        // loop over functions in the list
        ForEach(func, funclist)
        {
                // obtain name of variable
                var := VarList(MetaToObject(func));        // variable name in a one-element list
                
                func := MetaToObject(func);
                
                Check(Length(var)<=?1, "Plot2D: Error: expression is not a function of one variable: "
                        ~(PipeToString()Write(func))
                );
                // Allow plotting of constant functions
                Decide(Length(var)=?0, var:=[_dummy]);
                // store variable name if not already done so
                Decide(
                        optionshash["xname"] =? "",
                        optionshash["xname"] := ToString(VarList(MetaToObject(var))[1])
                );
                // store function name in options
                Append!(optionshash["yname"], PipeToString()Write(func));
                
                // compute the first point to see if it's okay
                c := range[1];
                
                fc := NM(Eval(Apply([var, func], [c])));
                
                Check(Number?(fc) |? fc=?Infinity |? fc=? -Infinity |? fc=?Undefined, 
                        "Plot2D: Error: cannot evaluate function "
                        ~(PipeToString()Write(func))
                        ~" at point "
                        ~(PipeToString()Write(c))
                        ~" to a number, instead got "
                        ~(PipeToString()Write(fc))
                        ~""
                );
                
                // compute all other data points
                Append!(allvalues,  Plot2Dgetdata(func, var, c, fc, delta, optionshash) );
        
                Decide(Verbose?(), Echo(["Plot2D: using ", Length(allvalues[Length(allvalues)]), " points for function ", func]), True);
        };
    
        // call the specified output backend
        Plot2DOutputs()[optionshash["output"]] @ [allvalues, optionshash];
};

//HoldArgument("Plot2D", "range");
//HoldArgument("Plot2D", "options");
HoldArgumentNumber("Plot2D", 2, 2);
HoldArgumentNumber("Plot2D", 3, 2);
HoldArgumentNumber("Plot2D", 3, 3);


/// this is the middle-level plotting routine; it generates the initial
/// grid, calls the adaptive routine, and gathers data points.
/// func must be just one function (not a list)
Plot2Dgetdata(func_, var_, xinit_, yinit_, deltax_, optionshash_) <--
{
        Local(i, a, fa, b, fb, c, fc, result);
        // initialize list by first points (later will always use Rest() to exclude first points of subintervals)
        result := [ [c,fc] := [xinit, yinit] ];
        
        For(i:=0, i<?optionshash["points"], i++)
        {
                [a,fa] := [c, fc];        // this is to save time but here a = xinit + i*deltax
                // build subintervals
                [b, c] := NM(Eval([xinit + (i+1/2)*deltax, xinit + (i+1)*deltax]));        // this is not computed using "a" to reduce roundoff error
                [fb, fc] := NM(Eval(MapSingle([var, func], [b, c])));
                result := Concat(result,
                        Rest(Plot2Dadaptive(func, var, [a,b,c], [fa, fb, fc], optionshash["depth"],
                                // since we are dividing into "points" subintervals, we need to relax precision
                                optionshash["precision"]*optionshash["points"] )));
        };
    
        result;
};

//////////////////////////////////////////////////
/// Plot2Dadaptive --- core routine to collect data
//////////////////////////////////////////////////
/*
        Plot2Dadaptive returns a list of pairs of coordinates [ [x1,y1], [x2,y2],...]
        All arguments except f() and var must be numbers. var is a one-element list containing the independent variable. The "a,b,c" and "fa, fb, fc" arguments are values of the function that are already computed -- we don't want to recompute them once more.
        See documentation (Algorithms.chapt.txt) for the description of the algorithm.
*/

Plot2Dadaptive(func_, var_, [a_,b_,c_], [fa_, fb_, fc_], depth_, epsilon_) <--
{
        Local(a1, b1, fa1, fb1);

        a1 := NM(Eval((a+b)/2));
        b1 := NM(Eval((b+c)/2));
        [fa1, fb1] := NM(Eval(MapSingle([var, func], [a1, b1])));
        Decide(
                depth<=?0 |?
                (
                  // condition for the values not to oscillate too rapidly
                  signchange(fa, fa1, fb) + signchange(fa1, fb, fb1) + signchange(fb, fb1, fc) <=? 2
                  &?
                  // condition for the values not to change too rapidly
                  NM(Eval(Abs( (fa-5*fa1+9*fb-7*fb1+2*fc)/24 ) ))        // this is the Simpson quadrature for the (fb,fb1) subinterval (using points b,b1,c), subtracted from the 4-point Newton-Cotes quadrature for the (fb,fb1) subinterval (using points a, a1, b, b1)
                          <=? NM(Eval( epsilon*(        // the expression here will be nonnegative because we subtract the minimum value
                    //(fa+fc+2*fb+4*(fa1+fb1))/12        // this is 1/4 of the Simpson quadrature on the whole interval
                        (5*fb+8*fb1-fc)/12        // this is the Simpson quadrature for the (fb,f1) subinterval
                        - Minimum([fa,fa1,fb,fb1,fc]) ) ) )
                ),
                // okay, do not refine any more
                [[a,fa], [a1,fa1], [b,fb], [b1,fb1], [c,fc]],
                // not okay, need to refine more
                Concat(
                        // recursive call on two halves of the interval; relax precision by factor of 2
                        Plot2Dadaptive(func, var, [a, a1, b], [fa, fa1, fb], depth-1, epsilon*2),        // Rest() omits the pair [b, fb]
                        Rest(Plot2Dadaptive(func, var, [b, b1, c], [fb, fb1, fc], depth-1, epsilon*2))
                )
        );
};

};        // LocalSymbols()


%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output




%mathpiper_docs,name="Plot2D",categories="Mathematics Procedures;Visualization"
*CMD Plot2D --- adaptive two-dimensional plotting
*STD
*CALL
        Plot2D(f(_x))
        Plot2D(f(_x), a->b)
        Plot2D(f(_x), a->b, option: value)
        Plot2D(f(_x), a->b, option: value, ...)
        Plot2D(list, ...)

*PARMS

{f(_x)} -- unevaluated expression containing one variable (function to be plotted)

{list} -- list of functions to plot

{a}, {b} -- numbers, plotting range in the $x$ coordinate

{option} -- atom, option name

{value} -- atom, number or string (value of option)

*DESC
The routine {Plot2D} performs adaptive plotting of one or several functions
of one variable in the specified range.
The result is presented as a line given by the equation $y=f(\_x)$.
Several functions can be plotted at once.
Various plotting options can be specified.
Output can be directed to a plotting program (the default is to use
{data}) to a list of values.

The function parameter {f(_x)} must evaluate to a MathPiper expression containing
at most one variable. (The variable does not have to be called {_x}.)
Also, {NM(f(_x))} must evaluate to a real (not complex) numerical value when 
given a numerical value of the argument {_x}. If the function {f(_x)} does not 
satisfy these requirements, an error is raised.

Several functions may be specified as a list and they do not have to depend on 
the same variable, for example, {{f(_x), g(_y)}}. The functions will be plotted 
on the same graph using the same coordinate ranges.

If you have defined a function which accepts a number but does not
accept an undefined variable, {Plot2D} will fail to plot it.
Use {NFunction} to overcome this difficulty.

Data files are created in a temporary directory {/tmp/plot.tmp/} unless otherwise requested.
File names
and other information is printed if {Verbose?()} returns {True} on using {Verbose()}.

The current algorithm uses Newton-Cotes quadratures and some heuristics for error 
estimation (see <*mathpiperdoc://Algo/3/1/*>). The initial grid of {points+1} 
points is refined between any grid points $a$, $b$ if the integral
$Integrate(x,a,b)f(_x)$ is not approximated to the given precision by
the existing grid.

Options are of the form {option: value}. Currently supported option names
are: "points", "precision", "depth", "output", "filename", "yrange". Option values
are either numbers or special unevaluated atoms such as {data}.
If you need to use the names of these atoms
in your script, strings can be used. Several option/value pairs may be specified 
(the function {Plot2D} has a variable number of arguments).

*        {yrange}: the range of ordinates to use for plotting, e.g.
{yrange=0->20}. If no range is specified, the default is usually to
leave the choice to the plotting backend. 
*        {points}: initial number of points (default 23) -- at least that
many points will be plotted. The initial grid of this many points will be
adaptively refined. 
*        {precision}: graphing precision (default $10^(-6)$). This is 
interpreted as the relative precision of computing the integral of 
$f(_x)-Minimum(f(_x))$ using the grid points. For a smooth, non-oscillating 
function this value should be roughly 1/(number of screen pixels in the plot).
*        {depth}: max. refinement depth, logarithmic (default 5) -- means 
there will be at most $2^depth$ extra points per initial grid point.
*        {output}: name of the plotting backend. Supported names: {data} (default).
The [data] backend will return the data as a list of pairs such as {[[x1,y1], [x2,y2], ...]}.
*        [filename]: specify name of the created data file. For example: [filename="data1.txt"].
The default is the name {"output.data"}.
Note that if several functions are plotted, the data files will have a number 
appended to the given name, for example {data.txt1}, {data.txt2}.

Other options may be supported in the future.

The current implementation can deal with a singularity within the plotting range 
only if the function {f(_x)} returns {Infinity}, {-Infinity} or
{Undefined} at the singularity.
If the function {f(_x)} generates a numerical error and fails at a
singularity, {Plot2D} will fail if one of the grid points falls on the
singularity.
(All grid points are generated by bisection so in principle the
endpoints and the {points} parameter could be chosen to avoid numerical
singularities.)

*WIN32



*SEE Verbose, NFunction, Plot3DS
%/mathpiper_docs






%mathpiper,name="Plot2D",subtype="automatic_test"

/* I stringified the results for now, as that is what the tests used to mean. The correct way to deal with this
 * would be to compare the resulting numbers to accepted precision.
 */
Verify(PipeToString()Write(Plot2D(_a,-1->1,output: data,points: 4,depth: 0)), "[[[-1,-1],[-0.5,-0.5],[0.0,0.0],[0.5,0.5],[1,1]]]");

%/mathpiper
