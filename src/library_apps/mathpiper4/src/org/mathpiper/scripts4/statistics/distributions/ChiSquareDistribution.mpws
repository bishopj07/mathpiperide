%mathpiper,def="ChiSquareDistribution"

/* Guard against distribution objects with senseless parameters
   Anti-nominalism */
   
RulebaseHoldArguments("ChiSquareDistribution",["x"]);

ChiSquareDistribution(m_RationalOrNumber?)::(m<=?0) <-- Undefined;

%/mathpiper



%mathpiper_docs,name="ChiSquareDistribution",categories="Mathematics Procedures;Statistics & Probability"
*CMD ChiSquareDistribution --- Chi square distribution
*STD
*CALL
        ChiSquareDistribution(p)

*PARMS

{p} -- number, probability of an event in a single trial

*DESC
A random variable has a ChiSquare distribution with probability {p} if
it can be interpreted as an indicator of an event, where {p} is the
probability to observe the event in a single trial.

Numerical value of {p} must satisfy $0<p<1$.

*Examples

*SEE BinomialDistribution, BernoulliDistribution, DiscreteUniformDistribution, ContinuousUniformDistribution, ExponentialDistribution, GeometricDistribution, NormalDistribution, PoissonDistribution, tDistribution
%/mathpiper_docs

 




%mathpiper,name="ChiSquareDistribution",subtype="in_prompts"

NM(PDF(ChiSquareDistribution(5), 7)) -> 0.07437126772;

NM(CDF(ChiSquareDistribution(5), 2)) -> 0.08208499862;

%/mathpiper


