%mathpiper,def="RulesByType;RemoveDollarSigns"

RulesByType(type, property) :=
{
	Local(body, bodyDescription, headDescription, index, manipulation, manipulationsCount, ruleInfo, ruleNames, ruleNamesSorted, thenString, guardDescription);
	
	RulebaseElementaryAlgebra();

	ruleNames := AssociationIndices(Select(?rulebaseElementaryAlgebra, Lambda([rule], StringContains?(ToString(rule[2][type]), ToString(property))  )));
	
	ruleNamesSorted := Sort(ruleNames);
	
	ForEach(ruleName, ruleNamesSorted)
	{
		ruleInfo := ?rulebaseElementaryAlgebra[ruleName];
		
		body := ruleInfo["Body"][1];
		
		If(List?(body))
		{
			bodyDescription := "";
			manipulationsCount := Length(body);
			
			thenString := " then ";
			
			index := 1;
			
			While(index <=? manipulationsCount)
			{
				manipulation := StringTrim(ToString(MetaToObject(body[index])));
	
				bodyDescription := bodyDescription ~ manipulation ~ Decide(index <? manipulationsCount, " followed by ", "");
				
				index++;
			};
		}
		Else
		{
			bodyDescription := StringTrim(ToString(MetaToObject(body)));
		};
		
		bodyDescription := StringReplace(bodyDescription, "==  ", "= ");
		bodyDescription := StringReplace(bodyDescription, "==", "=");
	
		headDescription := StringTrim(ToString(RemoveDollarSigns(ruleInfo["Head"])));
		
		headDescription := StringReplace(headDescription, "==", "=");
		
		If(ruleInfo["DescriptionGuard"] !=? None)
		{
			guardDescription := " :: " ~ ruleInfo["DescriptionGuard"];
		}
		Else If(ruleInfo["Guard"] !=? None)
		{
			guard := ruleInfo["Guard"][1];
			
			guard := guard /: 
			[
				a_ =? _unknown <- ` 'Unknown?(@a),
				Occurrence(a_, _unknown) !=? 0 <- ` 'UnknownIn?(@a),
				Occurrence(a_,_unknown) =? 0 <- ` 'UnknownNotIn?(@a)
				
			];
			guardDescription := " :: " ~ ToString(guard);
		}
		Else
		{
			guardDescription := "";
		};
		
		Echo(headDescription ~ guardDescription ~ " <- " ~ bodyDescription);
	};
};

RemoveDollarSigns(equation) :=
{
	equation := Substitute(ToAtom("==$"),ToAtom("==")) equation;
    equation := Substitute(ToAtom("+$"),ToAtom("+")) equation;
    equation := Substitute(ToAtom("-$"),ToAtom("-")) equation;
    equation := Substitute(ToAtom("*$"),ToAtom("*")) equation;
    equation := Substitute(ToAtom("/$"),ToAtom("/")) equation;
    equation := Substitute(ToAtom("^$"),ToAtom("^")) equation;
};
	
%/mathpiper

    %output,sequence="2",timestamp="2016-06-15 12:25:29.776",preserve="false"
      Result: True
.   %/output




In> RulesByType("Type", TEXTBOOK);

    %output,sequence="88",timestamp="2016-06-13 01:26:56.348",preserve="false"
      Result: True
      
      Side Effects:
      q_*1 <- q
      q_ + 0 <- q
      0 + q_ <- q
      1*q_ <- q
      q_ <- q*1
      q_ <- q + 0
      q_ <- 0 + q
      q_ <- 1*q
.   %/output



In> RulesByType("StructuralEffect", "ELIMINATE");

    %output,sequence="96",timestamp="2016-06-13 01:46:26.727",preserve="false"
      Result: True
      
      Side Effects:
      q_*s_ + r_*s_ :: Unknown?(s) <- (q + r)*s
      r_*q_ + q_ :: Unknown?(q) <- (r + 1)*q
      q_ + q_ :: Unknown?(q) <- 2*q
.   %/output

