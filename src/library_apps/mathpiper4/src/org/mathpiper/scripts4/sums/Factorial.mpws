%mathpiper,def="Factorial"
Factorial(x) :=
{
	If(x =? 0)
	{
		1;
	}
	Else If(x =? Infinity)
	{
		Infinity;
	}
	Else If(PositiveInteger?(x))
	{
		Check(x <=? 65535, "Factorial: Error: the argument " ~ ( PipeToString() Write(x) ) ~ " is too large, you may want to avoid exact calculation");
		MathFac(x);
	}
	Else If(Constant?(x) &? FloatIsInt?(x) &? x >? 0)
	{
		Factorial(Round(x));
	}
	Else If(Number?(x) &? NumericMode?())
	{
		InternalGammaNum(x+1);
	}
	Else If(RationalOrNumber?(x) &? Denominator(Rationalize(x)) =? 2)
	{
		x := Numerator(Rationalize(x));
		
		Check(Odd?(x), "The argument is invalid.");
		
		If(x >? 0)
		{
			Sqrt(Pi) * ( Factorial(x) / ( 2^x * Factorial((x-1)/2) ) );
		}
		Else
		{
			Sqrt(Pi) * ( (-1)^((-x-1)/2)*2^(-x-1) * Factorial((-x-1)/2) / Factorial(-x-1) );
		};
	}
	Else
	{
		Check(False, "The argument is invalid.");
	};
};

%/mathpiper





%mathpiper_docs,name="Factorial",categories="Mathematics Procedures;Combinatorics"
*CMD Factorial  --- factorial

*CALL
        Factorial(n)
        
*PARMS
{n} -- integer or half-integer

*DESC
The factorial function calculates the factorial of integer or half-integer numbers. For
nonnegative integers, $n! := n*(n-1)*(n-2)*...*1$. The factorial of
half-integers is defined via Euler's Gamma function, $z! := Gamma(z+1)$. If $n=0$ the function returns $1$.

The factorial function throws an exception if the argument is too large (currently the limit 
is $n < 65535$) because exact factorials of such large numbers are computationally expensive and most probably 
not useful. One can call {InternalLnGammaNum()} to evaluate logarithms of such factorials to desired precision.

*E.G.

In> Factorial(5)
Result: 120

In> 1 * 2 * 3 * 4 * 5
Result: 120;

In> Factorial(1/2)
Result: Sqrt(Pi)/2;

*SEE BinomialCoefficient, Product, Gamma, ***, Subfactorial
%/mathpiper_docs





%mathpiper,name="Factorial",subtype="automatic_test"

Verify(Factorial(0), 1);
Verify(Factorial(Infinity), Infinity);
Verify(Factorial(3), 6);
Verify(Factorial(3.0), 6);
Verify(Factorial(261) - 261 * Factorial(260), 0);
Verify(Factorial(300) / Factorial(250), 251***300);
Verify(Factorial(1/2), Sqrt(Pi) / 2);

%/mathpiper