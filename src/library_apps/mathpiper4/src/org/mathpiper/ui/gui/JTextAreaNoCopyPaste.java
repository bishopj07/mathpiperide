
package org.mathpiper.ui.gui;

import javax.swing.JTextArea;


public class JTextAreaNoCopyPaste extends JTextArea
{
    
    public JTextAreaNoCopyPaste()
    {
        super();
    }
        
    public JTextAreaNoCopyPaste(String text, int rows, int columns)
    {
        super(text, rows, columns);
    }
    
    public void copy()
    {
    }
    
    public void paste()
    {
    }       
}

