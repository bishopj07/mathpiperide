%mathpiper,def="AssignArray"

RulebaseHoldArguments("AssignArray", [listOrAssociationList, indexOrKey, newValue]);

UnFence("AssignArray", 3);


// Assign association lists.
RuleHoldArguments("AssignArray", 3, 1, String?(indexOrKey))
{
    Local(keyValuePair);

    keyValuePair := Assoc(indexOrKey, listOrAssociationList);

    Decide(keyValuePair =? Empty,

        DestructiveInsert(listOrAssociationList, 1 , [indexOrKey, newValue]),

        DestructiveReplace(keyValuePair, 2 , newValue)
    );

    True;
};



// Assign generic arrays.
RuleHoldArguments("AssignArray",3,1, And?( Equal?(Generic?(listOrAssociationList), True), Equal?(GenericTypeName(listOrAssociationList), "Array")))
{
    ArraySet(listOrAssociationList, indexOrKey, newValue);
};


RuleHoldArguments("AssignArray",3 , 2, True)
{
    DestructiveReplace(listOrAssociationList, indexOrKey, newValue);

    True;
};


%/mathpiper





%mathpiper_docs,name="AssignArray",categories="Programming Functions;Miscellaneous;Built In",access="private"
*CMD AssignArray --- auxiliary function to help assign arrays using :=

*CALL
        AssignArray(listOrAssociationList, indexOrKey, newValue);

*PARMS

{listOrAssociationList} -- a list or an association list

{indexOrKey} -- an index or a string which is a key

{newValue} -- the new value to place into the list or the association list


*DESC
This function will destructively change a list, an association list, or a
Generic object.



*E.G.
// Change a normal list.
In> list := [7,8,9]
Result> [7,8,9]

In> AssignArray(list, 2, "eight")
Result> True

In> list
Result> [7,"eight",9]



// Change an association list.
In> list := [["seven", 7], ["eight", 8], ["nine", 9]]
Result> [["seven",7],["eight",8],["nine",9]]

In> AssignArray(list, "eight", "EIGHT")
Result> True

In> list
Result> [["seven",7],["eight","EIGHT"],["nine",9]]



// Add an association to an association list.
In> list := [["seven", 7], ["eight", 8], ["nine", 9]]
Result> [["seven",7],["eight",8],["nine",9]]

In> AssignArray(list, "ten", 10)
Result> True

In> list
Result> [["ten",10],["seven",7],["eight",8],["nine",9]]

*SEE :=
%/mathpiper_docs