%mathpiper,def="CDF"

/* Evaluates distribution dst at point x
   known distributions are:
   1. Discrete distributions
   -- BernoulliDistribution(p)
   -- BinomialDistribution(p,n)
   -- DiscreteUniformDistribution(a,b)
   -- PoissonDistribution(l)
   -- HypergeometricDistribution(N, M)
   2. Continuous distributions
   -- ExponentialDistribution(l)
   -- NormalDistrobution(a,s)
   -- ContinuousUniformDistribution(a,b)
   -- tDistribution(m)
   -- GammaDistribution(m)
   -- ChiSquareDistribution(m)

  DiscreteDistribution(domain,probabilities) represent arbitrary
  distribution with finite number of possible values; domain list
  contains possible values such that
  Pr(X=domain[i])=probabilities[i].
  TODO: Should domain contain numbers only?
*/


/* Evaluates Cumulative probability function CDF(x)=Pr(X<x) */

//Retract("CDF", *);

//Discrete distributions.

10 # CDF(BernoulliDistribution(_p), x_Number?) <-- Decide(x<=?0,0,Decide(x>?0 And? x<=?1, p,1));
11 # CDF(BernoulliDistribution(_p), _x) <-- Hold(Decide(x<=?0,0,Decide(x>?0 And? x<=?1, p,1)));

10 # CDF(BinomialDistribution(_p,_n),m_Number?)_(m<?0) <-- 0;
10 # CDF(BinomialDistribution(_p,n_Integer?),m_Number?)_(m>?n) <-- 1;
10 # CDF(BinomialDistribution(_p,_n),_m) <-- Sum @ [ i, 0, Floor(m), PMF(BinomialDistribution(p,n),i)];

10 # CDF(DiscreteUniformDistribution( a_Number?, b_Number?), x_Number?)_(x<=?a) <-- 0;
10 # CDF(DiscreteUniformDistribution( a_Number?, b_Number?), x_Number?)_(x>?b) <-- 1;
10 # CDF(DiscreteUniformDistribution( a_Number?, b_Number?), x_Number?)_(a<?x And? x<=?b) <-- (x-a)/(b-a+1);
11 # CDF(DiscreteUniformDistribution( _a, _b), _x) <--Hold(Decide(x<=?a,0,Decide(x<=?b,(x-a)/(b-a),1)));

10 # CDF(PoissonDistribution(_l), x_Number?)_(x<=?0) <-- 0;
10 # CDF(PoissonDistribution(_l), _x) <-- Sum @ [i,0,x,PMF(PoissonDistribution(l),i)];

10 # CDF(ChiSquareDistribution(_m), _x) <-- IncompleteGamma(m/2,x/2)/Gamma(x/2);
10 # CDF(DiscreteDistribution( dom_List?, prob_List?), _x)   <--
      {
         Local(i,cdf,y);

         i := 1;
         cdf:=0;
         y:=dom[i];
         While(y<?x) {cdf:=cdf+prob[i];i++;};
         cdf;
      };

10 # CDF(HypergeometricDistribution( N_Number?, M_Number?, n_Number?), x_Number?)_(M <=? N And? n <=? N) <-- 
{
    Sum @ [i,0,x,PMF(HypergeometricDistribution(N, M, n),i)];
};


//Continuous distributions.

10 # CDF(NormalDistribution(_m,_s), _x) <-- 1/2 + 1/2 * ErrorFunction((x - m)/(s*Sqrt(2))); //See http://en.wikipedia.org/wiki/Normal_distribution.

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output




%mathpiper_docs,name="CDF",categories="Mathematics Functions;Statistics & Probability"
*CMD CDF --- cumulative distribution function
*STD
*CALL
        CDF(dist,x)

*PARMS
{dist} -- a distribution type

{x} -- a value of random variable

*DESC
The cumulative distribution function (CDF) (or just distribution function) describes the 
probability distribution of a real-valued random variable X. For every real number x, 
the CDF of X represents the probability that the random variable X takes on a value less 
than or equal to x.


*SEE PDF, Expectation
%/mathpiper_docs