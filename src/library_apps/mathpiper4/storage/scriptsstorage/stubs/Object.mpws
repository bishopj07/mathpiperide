%mathpiper,def="Object"

RulebaseHoldArguments("Object",[pred,x]);
RuleHoldArguments("Object",2,0,Apply(pred,[x])=?True) x;

%/mathpiper



%mathpiper_docs,name="Object",categories="Programming Functions;Variables"
*CMD Object --- create an incomplete type
*STD
*CALL
        Object("pred", exp)

*PARMS

{pred} -- name of the predicate to apply

{exp} -- expression on which "pred" should be applied

*DESC

This function returns "obj" as soon as "pred" returns {True} 
when applied on "obj". This is used to declare
so-called incomplete types.

*E.G.

In> a := Object("Number?", x);
Result: Object("Number?",x);

In> Eval(a);
Result: Object("Number?",x);

In> x := 5;
Result: 5;

In> Eval(a);
Result: 5;

*SEE NonObject?
%/mathpiper_docs