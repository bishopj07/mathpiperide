//Copyright (C) 2008 Ted Kosan (license information is at the end of this document.)
package org.mathpiper.ide.mathpiperplugin;

import org.gjt.sp.jedit.*;
import org.gjt.sp.jedit.gui.DockableWindowManager;
import org.mathpiper.ui.gui.consoles.GraphicConsole;



/**
 * The Piper plugin
 * 
 * @author Ted Kosan
 */
public class MathPiperPlugin extends EditPlugin implements EBComponent{
	public static final String NAME = "mathpiper";
	public static final String OPTION_PREFIX = "options.mathpiper.";
	
	public static GraphicConsole console;
	
	public void start()
	{
		
		//jEdit.getActiveView().getDockableWindowManager().addDockableWindow(org.mathpiper.ide.mathpiperplugin.MathPiperPlugin.NAME);
		//jEdit.getActiveView().getDockableWindowManager().showDockableWindow( "mathpiper" );
		//System.out.println("************************************************MathPiper plugin started...");
		EditBus.addToBus(this);
		
		org.mathpiper.interpreters.Interpreter interpreter = org.mathpiper.interpreters.Interpreters.getSynchronousInterpreter();
		
		org.mathpiper.lisp.Environment environment = interpreter.getEnvironment();

        org.mathpiper.interpreters.Interpreters.addOptionalFunctions(environment,"org/mathpiper/builtin/functions/optional/");
        
        org.mathpiper.interpreters.Interpreters.addOptionalFunctions(environment,"org/mathpiper/builtin/functions/nongwtcore/");
        
        org.mathpiper.interpreters.Interpreters.addOptionalFunctions(environment,"org/mathpiper/builtin/functions/plugins/jfreechart/");
        
        console = new org.mathpiper.ui.gui.consoles.GraphicConsole();
		
	}//end method.
	
	
	public void handleMessage(EBMessage msg)
	{
		//System.out.println("************************************************MathPiper plugin received editor message... "+ msg);
		
		//Uncomment the following 'if' statement to open the MathPiperConsole plugin immediate after MathPiperIDE is launched.
		/*
		if (msg instanceof org.gjt.sp.jedit.msg.EditorStarted) {
			jEdit.getActiveView().getDockableWindowManager().addDockableWindow(org.mathpiper.ide.mathpiperplugin.MathPiperPlugin.NAME);
		}
		*/

	}
	

    public static void setHaltButtonState(boolean state)
    {
	    console.setHaltButtonEnabledState(state);
    }

    public static boolean getHaltButtonState()
    {
	    return console.getHaltButtonEnabledState();
    }
	
	
}//end class.

/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
